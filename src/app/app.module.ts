import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { PivotViewModule } from '@syncfusion/ej2-angular-pivotview';
import { GridModule } from '@syncfusion/ej2-angular-grids';
import { ChartAllModule } from '@syncfusion/ej2-angular-charts';

import { AppComponent } from './app.component';
import { PivotComponent } from './pivot/pivot.component';
import { ChartsComponent } from './charts/charts.component';
import { DataGridComponent } from './data-grid/data-grid.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DetailComponent } from './detail/detail/detail.component';

@NgModule({
  declarations: [
    AppComponent,
    PivotComponent,
    ChartsComponent,
    DataGridComponent,
    NotFoundComponent,
    DetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PivotViewModule,
    GridModule,
    ChartAllModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
