import { Component, OnInit, ViewChild } from '@angular/core';
import { PageService, SortService, FilterService, GroupService, EditService, CommandColumnService, CommandModel, SaveEventArgs, GridComponent, Column, ToolbarService } from '@syncfusion/ej2-angular-grids';
import { LocalService } from '../services/local/local.service';
import { data } from 'src/assets/json/data';


@Component({
  selector: 'app-data-grid',
  templateUrl: './data-grid.component.html',
  styleUrls: ['./data-grid.component.scss'],
  providers: [PageService, SortService, EditService, CommandColumnService, ToolbarService]
})
export class DataGridComponent implements OnInit {

  @ViewChild('grid') grid!: GridComponent;
  public data: Object[] = [];
  public pageSettings!: Object;
  public filter: Boolean = true;
  public editSettings!: Object;
  public toolbar!: string[];
  public orderidrules!: Object;
  public customeridrules!: Object;
  public freightrules!: Object;
  public editparams!: Object;
  public commands!: CommandModel[];

  constructor(
    private local: LocalService,
  ) { }

  // actionComplete(args: SaveEventArgs) {
  actionComplete(args: any) {
    // console.log(args);
    let syncData = this.local.getLocalSingle('syncData');
    if (args.requestType === 'save') {
      if (args.action === 'add') {
        syncData.unshift({ ...args.data });
      } else if (args.action === 'edit') {
        let objIndex = syncData.findIndex(((obj: any) => obj.OrderID == args.data.OrderID));
        syncData[objIndex] = { ...args.data }
      }
    }
    if (args.requestType === 'delete') {
      syncData = syncData.filter((obj: any) => obj.OrderID != args.data[0].OrderID);
    }
    // console.log(syncData);
    this.local.setLocalSingle('syncData', syncData);
  }

  ngOnInit(): void {
    let syncData = this.local.getLocalSingle('syncData');
    if (syncData) {
      this.data = syncData;
    } else {
      this.data = data;
      this.local.setLocalSingle('syncData', data);
    }

    this.pageSettings = { pageCount: 5, pageSize: 10 };
    this.toolbar = ['Add', 'Edit', 'Delete'];
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Normal', allowEditOnDblClick: false };


    this.orderidrules = { required: true };
    this.customeridrules = { required: true };
    this.freightrules = { required: true };
    this.editparams = { params: { popupHeight: '300px' } };
    this.commands = [{ type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
    { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },
    { type: 'Save', buttonOption: { iconCss: 'e-icons e-update', cssClass: 'e-flat' } },
    { type: 'Cancel', buttonOption: { iconCss: 'e-icons e-cancel-icon', cssClass: 'e-flat' } }];
  }
}
