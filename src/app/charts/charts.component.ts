import { Component, OnInit, ViewEncapsulation } from '@angular/core';
// import { ILoadedEventArgs, ChartTheme } from '@syncfusion/ej2-angular-charts';
// import { Browser } from '@syncfusion/ej2-base';
import { data } from 'src/assets/json/data';

import {
  CategoryService,
  LineSeriesService,
  TooltipService,
  LegendService
} from '@syncfusion/ej2-angular-charts';
import { LocalService } from '../services/local/local.service';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [CategoryService, LineSeriesService, TooltipService, LegendService]
})
export class ChartsComponent implements OnInit {

  public chartData: Object[] = [];
  public title!: string;
  public primaryXAxis!: Object;
  public primaryYAxis!: Object;

  public marker: Object = {
    visible: true,
    height: 10,
    width: 10
  };
  public tooltip: Object = {
    enable: true
  };

  constructor(
    private local: LocalService,
  ) { }

  ngOnInit(): void {
    let syncData = this.local.getLocalSingle('syncData');
    if (syncData) {
      this.chartData = syncData;
    } else {
      this.chartData = data;
      this.local.setLocalSingle('syncData', data);
    }

    // this.chartData = [
    //   { x: new Date(2005, 0, 1), y: 21 }, { x: new Date(2006, 0, 1), y: 24 },
    //   { x: new Date(2007, 0, 1), y: 36 }, { x: new Date(2008, 0, 1), y: 38 },
    //   { x: new Date(2009, 0, 1), y: 54 }, { x: new Date(2010, 0, 1), y: 57 },
    //   { x: new Date(2011, 0, 1), y: 70 }
    // ];
    // this.chartData = data;
    // console.log(this.chartData);

    // this.primaryXAxis = {
    //   valueType: 'DateTime',
    //   labelFormat: 'y',
    //   intervalType: 'Years',
    //   edgeLabelPlacement: 'Shift',
    //   majorGridLines: { width: 0 }
    // };
    this.primaryXAxis = {
      rangePadding: 'Additional',
      valueType: 'Category',
      title: 'Assignee'
    };
    this.primaryYAxis = {
      labelFormat: '{value}%',
      rangePadding: 'None',
      minimum: 0,
      maximum: 100,
      interval: 20,
      lineStyle: { width: 0 },
      majorTickLines: { width: 0 },
      minorTickLines: { width: 0 }
    };
    this.title = 'Sync Fusion Chart Demo';
  }
}
