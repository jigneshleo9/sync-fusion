import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class LocalService {

  constructor(@Inject(PLATFORM_ID) private platformId: Object) { }

  getLocal(modulee: any, key: any) {

    if (isPlatformBrowser(this.platformId)) {
      let obj = localStorage.getItem(modulee);
      let modObj;
      if (obj) {
        modObj = JSON.parse(obj);
      } else {
        modObj = {};
        localStorage.setItem(modulee, JSON.stringify(modObj));
      }
      if (key != undefined) return modObj[key];
      return modObj;
    }
  }

  setLocal(modulee: any, key: any, data: any) {
    if (isPlatformBrowser(this.platformId)) {
      let obj = localStorage.getItem(modulee);
      let modObj;
      if (obj) {
        modObj = JSON.parse(obj);
      } else {
        modObj = {};
      }
      modObj[key] = data;
      localStorage.setItem(modulee, JSON.stringify(modObj));
    }
  }

  removeLocal(modulee: any, key: any) {
    if (isPlatformBrowser(this.platformId)) {

      let obj = localStorage.getItem(modulee);
      let modObj;
      if (obj) {
        modObj = JSON.parse(obj);
        if (modObj[key]) {
          modObj[key] = undefined;
        }
        localStorage.setItem(modulee, JSON.stringify(modObj));
      }
    }
  }

  setLocalSingle(key: any, val: any) {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem(key, JSON.stringify(val));
    }
  }

  getLocalSingle(key: any) {
    if (isPlatformBrowser(this.platformId)) {
      let obj = localStorage.getItem(key);
      if (obj) {
        return JSON.parse(obj);
      }
    }
  }

  removeLocalSingle(key: any) {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.removeItem(key);
    }
  }

  removeAll() {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.clear();
    }
  }

}
