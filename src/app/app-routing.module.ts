import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PivotComponent } from './pivot/pivot.component';
import { ChartsComponent } from './charts/charts.component';
import { DataGridComponent } from './data-grid/data-grid.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { DetailComponent } from './detail/detail/detail.component';

const routes: Routes = [
  {
    path: '',
    component: DataGridComponent,
  },
  {
    path: 'data-grid',
    component: DataGridComponent,
  },
  {
    path: 'pivot',
    component: PivotComponent,
  },
  {
    path: 'charts',
    component: ChartsComponent,
  },
  {
    path: 'detail/:id',
    component: DetailComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
