import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { IDataOptions, GroupingBarService, PivotView, FieldListService, IDataSet } from '@syncfusion/ej2-angular-pivotview';
import { GridSettings } from '@syncfusion/ej2-pivotview/src/pivotview/model/gridsettings';
import { enableRipple } from '@syncfusion/ej2-base';
enableRipple(false);
import { data } from 'src/assets/json/data';
import { LocalService } from '../services/local/local.service';

declare var require: any;
// let Pivot_Data: IDataSet[] = require('./Pivot_Data.json'); 
let Pivot_Data: IDataSet[] = data;

@Component({
  selector: 'app-pivot',
  templateUrl: './pivot.component.html',
  styleUrls: ['./pivot.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [GroupingBarService, FieldListService]
})
export class PivotComponent implements OnInit {
  public data: IDataSet[] = [];
  public dataSourceSettings!: IDataOptions;
  public gridSettings!: GridSettings;

  @ViewChild('pivotview')
  public pivotObj!: PivotView;

  constructor(
    private local: LocalService,
  ) { }

  ngOnInit(): void {
    let syncData = this.local.getLocalSingle('syncData');
    if (syncData) {
      this.data = syncData;
    } else {
      this.data = Pivot_Data;
      this.local.setLocalSingle('syncData', data);
    }

    // console.log(Pivot_Data);

    this.gridSettings = {
      columnWidth: 140
    } as GridSettings;

    this.dataSourceSettings = {
      enableSorting: true,
      dataSource: this.data,
      expandAll: false,
      columns: [{ name: 'OrderDate' }],
      rows: [{ name: 'CustomerID' }],
      values: [{ name: 'Freight' }],
      // filters: [{ name: 'Product_Categories', caption: 'Product Categories' }],
      formatSettings: [{ name: 'OrderDate', format: 'dd/MM/yyy', type: 'date' }],
    };
  }
}
